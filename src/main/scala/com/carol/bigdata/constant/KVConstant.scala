package com.carol.bigdata.constant

object KVConstant {
    // 基本字段
    val cf: String = "value"
    val keyCF: String = "key"
    val timeField: String = "time"
    val gameField: String = "game_id"
    val propertiesField: String = "properties"
    val idField: String = "uid"
    val idTypeField: String = "uid_type"
    val eventField: String = "event"
    val timezoneFiled: String = "timezone"
    val keyColumns: List[String] = List(gameField, idField)
    val dateKeyColumns: List[String] = timeField +: keyColumns
    val gameKeyColumns: List[String] = List(gameField)
    val accountKeyColumns: List[String] = List()

    // 基本表及标识符
    // action 表
    val actionTable: String = "event:action"
    val loginPattern: String = "$login"
    val orderPattern: String = "$order"
    // 中间表
    val userProfileTable: String = "user_profile"

    val writeKeyCols: List[String] = List(gameField, timeField, idField)


    def main(args: Array[String]): Unit = {

    }
}
