package com.carol.bigdata.task.model.algo

object ModelUtil {

    // 构建模型
    def buildModel(name: String = "LR"): ModelTrait = {
        val modelName = name.toUpperCase()
        modelName match {
            case "LR" => new LR
            case "DT" => new DT
            case "RF" => new RF
            case "SVM" => new SVM
            case _ => new LR
        }
    }
}
