package com.carol.bigdata.task.model.algo

import org.apache.spark.ml.classification.{LogisticRegression, OneVsRest, OneVsRestModel}
import org.apache.spark.ml.param.{Param, ParamMap}
import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
import org.apache.spark.ml.{Pipeline, PipelineModel}


class LR extends ModelTrait {

    // 逻辑回归默认配置
    var maxIter: Int = 100
    var regParam: Double = 0.001
    val labelCol: String = "label"
    val featuresCol: String = "features"
    val rawPredictionCol: String = "rawPrediction"
    val predictionCol: String = "prediction"
    val regress: LogisticRegression = new LogisticRegression()
        .setMaxIter(maxIter)
        .setRegParam(regParam)
        .setFeaturesCol(featuresCol)
        .setLabelCol(labelCol)
        .setRawPredictionCol(rawPredictionCol)
        .setPredictionCol(predictionCol)

    // 交叉验证配置
    var tuneMaxIter: Array[Int] = Array(1000, 10000, 50000)
    var tuneRegParam: Array[Double] = Array(0.1, 0.01)


    // 配置模型默认参数
    override def init(params: Map[String, Any]): Unit = {
        maxIter = params.getOrElse("maxIter", maxIter.asInstanceOf[Any]).asInstanceOf[Int]
        regParam = params.getOrElse("regParam", regParam.asInstanceOf[Any]).asInstanceOf[Double]
        tuneMaxIter = params.getOrElse("tuneMaxIter", tuneMaxIter.asInstanceOf[Any]).asInstanceOf[Array[Int]]
        tuneRegParam = params.getOrElse("tuneRegParam", tuneRegParam.asInstanceOf[Any]).asInstanceOf[Array[Double]]
    }


    // 构建pipeline模型
    override def buildPipeline(featuresCol: String = "features",
                               labelCol: String = "label",
                               rawPredictionCol: String = "rawPrediction",
                               predictionCol: String = "prediction",
                               objective: String = "binary",
                               numClass: Int = 2): Pipeline = {
        // 设置已调优的参数
        val model: LogisticRegression = regress
            .setMaxIter(maxIter)
            .setRegParam(regParam)
            .setFeaturesCol(featuresCol)
            .setLabelCol(labelCol)
            .setRawPredictionCol(rawPredictionCol)
            .setPredictionCol(predictionCol)
        println(model.extractParamMap)
        println(model.explainParams)

        val ovrTree: OneVsRest = buildOvrTree(model,
            featuresCol, labelCol, rawPredictionCol, predictionCol, objective)

        // 构建pipeline
        val pipeline: Pipeline = new Pipeline()
            .setStages(if (objective == "binary") Array(model) else Array(ovrTree))

        pipeline
    }

    // 交叉验证
    override def buildValidator(pipeline: Pipeline,
                                seed: Int = 1,
                                numFolds: Int = 2,
                                parallelNum: Int = 2,
                                objective: String = "binary"): CrossValidator = {
        val gridBuilder = new ParamGridBuilder()
            .addGrid(regress.maxIter, tuneMaxIter)
            .addGrid(regress.regParam, tuneRegParam)

        val cv = buildValidatorFromGrid(pipeline, gridBuilder, seed, numFolds, parallelNum, objective)

        cv
    }

    // 更新微调参数
    override def updateTuneParams(bestParamsMap: ParamMap): Unit = {
        maxIter = bestParamsMap.getOrElse(regress.maxIter, maxIter)
        regParam = bestParamsMap.getOrElse(regress.regParam, regParam)
    }
    override def updateTuneParamsFromCV(crossModel: CrossValidatorModel,
                                        maxBy: Boolean = true,
                                        objective: String = "binary"): Unit = {
        val paramMapScores = crossModel.getEstimatorParamMaps.zip(crossModel.avgMetrics)
        val bestParamsMap = if (maxBy) paramMapScores.maxBy(_._2)._1 else paramMapScores.minBy(_._2)._1
        val bestModelId = {
            if (objective.toLowerCase.contains("binary")) crossModel.bestModel.asInstanceOf[PipelineModel].stages.last.uid
            else crossModel.bestModel.asInstanceOf[PipelineModel].stages.last.asInstanceOf[OneVsRestModel].getClassifier.uid
        }
        val iteParam = new Param[Int](bestModelId, name = "maxIter", doc = "maxIter")
        val rgParam = new Param[Double](bestModelId, name = "regParam", doc = "regParam")
        maxIter = bestParamsMap.getOrElse(iteParam, maxIter)
        regParam = bestParamsMap.getOrElse(rgParam, regParam)
    }

}
