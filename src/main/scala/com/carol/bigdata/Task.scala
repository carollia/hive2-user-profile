package com.carol.bigdata

import com.carol.bigdata.task.feature.{CalEnvTag, CalFavorTag, CalPayTag, cag}
import com.carol.bigdata.task.label
import com.carol.bigdata.task.model.train
import org.apache.spark.sql.SparkSession

object Task {
   /*
   项目背景: 调整每日个人暗补区间, 提升留存率
   实现方案:
   1.每日计算用户特征(样本), 用户-留存表(标签) [保留最近30天]
   2.每天训练一次模型, 进行离线预测, 写入hbase
   */
    def run(hbaseParams: Map[String, String],
            spark: SparkSession,
            statDay: String,
            game: String): Unit = {
        /* --------- 通用特征 --------- */
        // 环境特征 (env_tag)
        CalEnvTag.run(hbaseParams, spark, statDay, game)
        // 偏好特征 (favor_tag)
        CalFavorTag.run(hbaseParams, spark, statDay, game)
        // 付费特征 (pay_tag)
        CalPayTag.run(hbaseParams, spark, statDay, game)
        /* --------- 应用特殊特征 --------- */
        // 社交特征 (social_tag)
        cag.calSocialTag.run(hbaseParams, spark, statDay, game)
        // 策略特征 (strategy_tag)
        cag.calStrategyTag.run(hbaseParams, spark, statDay, game)
        // 调整特征 (例如:游戏的暗补区间 adjust_tag)
        cag.calAdjustTag.run(hbaseParams, spark, statDay, game)

        /*
        计算留存标签
        */
        label.calRetentionLabel.run(hbaseParams, spark, statDay, game, Config.retentionLabelList)

        /*
        模型训练和预测
        */
        train.TrainRetention.run(hbaseParams, spark, game)
    }
}
