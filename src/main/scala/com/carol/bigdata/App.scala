package com.carol.bigdata

import com.carol.bigdata.utils.{Flag, HBaseUtil, TimeUtil}
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession


object App {
    
    def main(args: Array[String]): Unit = {
        Flag.Parse(args)
        val spark = SparkSession.builder()
                                .config(conf = Config.sparkConf)
                                .enableHiveSupport()
                                .getOrCreate()
        // hive地址相关配置需在SparkSession创建后再进行配置，否则可能因为各个配置文件会将地址覆盖掉导致进程停滞
        //spark.sparkContext.getConf
        //     .set("hive.metastore.uris", HiveMetaStoreUris)
        //     .set("spark.sql.warehouse.dir", sparkWarehouseDir)
        val sc: SparkContext = spark.sparkContext
        sc.setLogLevel(Config.logLevel)
        val hbaseParams: Map[String, String] = sc.broadcast(Config.hbaseParams).value
        println(s"Config.gameList: ${Config.gameList}, hbaseParams:${ hbaseParams }")
        val userProfileTable = "user_profile"
        val familyList = List("key",  "info", "env_tag", "favor_tag", "pay_tag", "strategy_tag", "social_tag", "adjust_tag")
        // todo: 1.获取游戏列表和对应时区(或者传入字符串数组)
        // todo: 2.创建用户画像表model.profile_xx,每个游戏创建一个用户画像(设置TTL为30天)
        // todo: 3.计算不同特征tag, 写入用户画像表
        for (game <- Config.gameList) {
            // 创建表
            HBaseUtil.createTable(hbaseParams, userProfileTable, familyList)
            // 获取统计日(昨日)
            val statDay = TimeUtil.getDeltaDay(-1)
            // 计算不同特征tag, 写入用户画像表
            TimeUtil.timer(Task.run(hbaseParams, spark, statDay, game))
        }
        spark.stop()
    }
    
}
